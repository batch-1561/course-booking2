//identify the components that will be used for this page.
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

//we will declare 'states' for our from components for us to be able to 'access and manage' the values in each of the from elements.

const data = {
	title: 'Welcome to Login',
	content: 'Sign in your account below'
}

//create a function that will describe the structure of the page.
export default function Login() {

	const {user, setUser} = useContext(UserContext);

	//declare an 'initial/default' state for our form of elements.
	//Bind/Lock the Form elements to the desired states
	//Assign the states to thier respective components
	//syntax: const/let [getter,setter] = useState()

	const [email, setEmail ] = useState('');
	const [password, setPassword ] = useState('');
	//apply the conditinal rendering to the button component for its current states.

	//validation are we going run on the email.
	//format: @ dns
	//search a character within a string
	let addressSign = email.search('@')
	//if the search m,ethod find no match inside the string it will return to -1
	//tips if your going to pass down the multiple values on the query. contain inside an array. includes()
	let dns = email.search('.com')


	//declare state for the login button
	const [isActive, setIsActive] = useState(false);
	const[isValid, setIsValid] = useState(false);
	
	//create a side effect taht will make our page reactive.
	useEffect(() => {
		//Create a condition that will evaluate the format of the email.
		//if the value is -1 (no match found inside the string)
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false);
		}

	},[email, password, addressSign, dns])


	//create a simulation that will help us to identify the workflow of the login page
	const loginUser = async (event) => {
		event.preventDefault();

		//send a request to verify if the user.s identity is true.
		//syntax: fetch (url, option)
		fetch('https://frozen-tundra-41775.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json()).then(dataNaJson => {
			//console.log(dataNaJSON.accessToken) //token
			let token = dataNaJson.accessToken;
			//console.log(token); //who?

			//create a control structure to give a proper response to the user.
			if (typeof token !== 'undefined') {
				//promp ng message kay user

				//The produce token, save it on the browser storage. (storage area)
				//to save an item in the local storagr object of the browser .... use setItem( key, value)
										//key ,value
				localStorage.setItem('accessToken',token);

				fetch('https://frozen-tundra-41775.herokuapp.com/users/details', {
				  headers: {
				    authorization: `Bearer ${token}`
				  }
				}).then(res => res.json()).then(convertedData => {
				   
				  if (typeof convertedData._id !== "undefined") {
				    setUser({
				      id: convertedData._id,
				      isAdmin: convertedData.isAdmin
				    });

				    Swal.fire({
				    	icon: 'success',
				    	title: 'Login Successful',
				    	text: 'Welcome!'
				    })

				   
				  } else {
				      //if the condition above is not met.
				    setUser({
				      id: null,
				      isAdmin: null
				    });
				  }
				  
				});


				
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Check your Credentials',
					text: 'Contact Admin if problem persist'
				})
				
			}

		})


		
	};

	return(
		user.id 
		?
			<Navigate to="/courses" replace={true} />
		:

		<>
			<Hero bannerData={data} />
			<Container>
				<h1 className="text-center">Login Form</h1>
				<Form onSubmit={e => loginUser(e)}>
			{/*Email field*/}
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
						type="email" 
						placeholder="Enter Email Here" required
						value={email}
						onChange={e => {setEmail(e.target.value)}}

						/>
						{
							isValid ?
								<h6 className="text-success">Email is Valid</h6>
							:
								<h6 className="text-mute">Email is Invalid</h6>
						}
						
						
					</Form.Group>

			{/*Password field*/}
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
						type="password" 
						placeholder="Enter password Here" required
						value={password}
						onChange={e => {setPassword(e.target.value)}}
						/>
					</Form.Group>

					{
						isActive ?
						    <Button className="btn-block" 
						    variant="success" 
						    type="submit"
						    >
						    Login
						    </Button>
						:
							<Button className="btn-block" 
							variant="secondary" 
							disabled
							>
							Login
							</Button>
					}

					

					
				</Form>
			</Container>
		</>	
	);
};
