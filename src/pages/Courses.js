// identify which componenet will be display
import { useState, useEffect } from 'react';
import Hero from './../components/Banner'
import CourseCard from './../components/CourseCard';
import { Container, Row }from 'react-bootstrap'

const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'
}

// Catalog all active Courses
	// 1. Need container component for each course that we will retrieve from the datbase
	//2. Declare state for the courses collection byt default it is an empty array
	// 3. create a side effect that will send a request to our backend proj.
	// 4. pass down the retrieved resources inside the component as props.

export default function Courses () {

	// this array/storage will be used to saved our active course components for all active courses.
	const [coursesCollection, setCourseCollection] = useState([]);

	// create an effect using our effect hook, provide a list of dependencies.
	// the effect that we are going to create is fetching data from the server
	useEffect(() => {
		// fetch() => is a JS methods wc allows us to pass/create a request to an API.
		// SYNTAX: fetch(<request URL>, {OPTIONS})
		// Get HTTP METHOD especially since we do not need to

		// remeber that once you send a request to an endpoint, a promise is returned as a result.
		// A promise has 3 satets: (fulfilled, reject, pending)

		// wee need to be able to handle the outcome of the promise.
		// wee need to make the response usable on the frontend side
		// upon converting the fetched data to a json format a new promise will be executed
		fetch('https://frozen-tundra-41775.herokuapp.com/courses//')
		.then(res => res.json())
		.then(convertedData => {
			// console.log(convertedData);
			// we want resources to be displayed in the page
			// since the dta is stored in an array storage, we will iterate the array to explicitly get each document one by one.
			// there will be multiple course card component that will renedered that corresponds to each document retrieved from the databse. we need to itilize a storage
			// key attribute as a referernce to avoid rendering duplicates of the same element/object.
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />	
				)
			}))
		});
	},[]);

	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				<Row>
				{coursesCollection}
				</Row>
			</Container>
		</>
	);
};
