//acquire all the components that will make up the home page. (hero section, highlights)
import Banner from './../components/Banner'
import Highlights from './../components/Highlights'
import { Container } from 'react-bootstrap';

//Lets create a data object that will describe the content of the hero section
const data = {
	title: 'Welcome to the Home Page',
	content: 'Opportunities for everyone, everywhere'

}

//identify first how you want the landing page to be structured.

//when warpping multiple adjacent JSX elements
	//=> div element
	//=> React Fragment - Group elements
						// - invisible div
						// - Container
			//syntax: long <React.Fragment> || <Fragment>
				//short <> </>
export default function Home() {
	return(
		<div>
			<Banner bannerData={data}/>
			<Highlights />
		</div>
	);
};