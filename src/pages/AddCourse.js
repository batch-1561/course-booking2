//identify the components that will used for this page
import Hero from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2'; 

const data = {
  title: 'Welcome to Login',
  content: 'Sign in your account below'
}
//create a function that will describe the structure of the page. 
export default function AddCourse() {

	//Create a simulation that will help us identify the workflow of the login page. 
	const addCourse = (event) => {
		event.preventDefault(); 
		return(
			Swal.fire({
				icon: 'success',
				title: 'Added Course Successfully',
				text: 'press [OK] to continue!'
			})
		);
	};

	return(
	   <>
			<Hero bannerData={data} />

			<Container>
				<h1 className="text-center">Add Course </h1>
				<Form onSubmit={e => addCourse(e)}>
				    {/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name: </Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter Course Name here.."
							required
						/>
					</Form.Group>

					{/*Course Desciption Field*/}
					<Form.Group>
						<Form.Label>Description: </Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter Course description here.."
							required
						/>
					</Form.Group>

				{/*Course Price Field*/}
					<Form.Group>
						<Form.Label>Cost: </Form.Label>
						<Form.Control 
							type="number"
							placeholder="Enter Course cost here.."
							required
						/>
					</Form.Group>

					<Button
					  className="btn-block" 
					  variant="success"
					  type="submit"
					>
					 Login
					</Button>
				</Form>
			</Container>

		</>
	);
};
