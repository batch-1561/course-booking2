// This will serve as a page whenever the client would want to select a single course/item from the catalog
import { useState, useEffect } from 'react'
import Hero from './../components/Banner';
// Grid system Card, Button

import {Row, Col, Card, Button, Container} from 'react-bootstrap';

// declare a state for the course details. we need to use the correct hook.

// import sweetalert
import Swal from 'sweetalert2';

// routing component
import { Link, useParams } from 'react-router-dom';

const data = {
	title: 'Welcome to b156 Booking-App',
	content: 'Check out our School Campus'
}

export default function CourseView() {

	// state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null

	});

	// retrieve the data from the url to extract the course id that will be used as a reference to determine wc course will be displayed by the page. acquire a 'Hook' wc will allow us to manage/access the data in the browser URL.

	// using the parameter hook(useParams) -> this will be provided by 'react-router-dom'

	// (useState, useEffect, useParams)

	// the useParams will return an object

	console.log(useParams());

	// extract the value from the path cariables by destructuring the object

	const {id} = useParams()
	console.log(id);

	// Create a side effect which will send a request to our back end API for course-booking. use the proper hook('effect hook')
	useEffect(() => {

		// send a request to our API ti retrieve information about the course.
		// syntax: fetch('<URL address>','<OPTIONS>')
		fetch(`https://frozen-tundra-41775.herokuapp.com/courses/${id}`).then(res => res.json())
		.then(convertedData => {
			// console.log(convertedData);
			// the data that we retrive from the databse we need to contain it
			// call the setter to change the state of the course info for this page
			setCourseInfo({
				name: convertedData.name,
				description:convertedData.description,
				price:convertedData.price 
			})

		});
	},[id])


	const enroll = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Enrolled Successfully!',
				text: 'Thank you for enrolling to this course'
			})

		);
	};	
	return (
	<>
		<Hero bannerData={data} />
		<Row>
			<Col>
				<Container>
					<Card className="text-center">
						<Card.Body>
					{/*course name*/}
							<Card.Title>
						{/*course desc*/}
								<h2> {courseInfo.name} </h2>
							</Card.Title>
							<Card.Subtitle>
								<h6 className="my-4"> Description: </h6>
							</Card.Subtitle>
							<Card.Text>
								{courseInfo.description}		
							</Card.Text>
						{/*course price*/}
								PHP: {courseInfo.price}
						</Card.Body>

						<Button variant="warning" className="btn-block" onClick={enroll}>
							Enroll
						</Button>

						<Link className="btn btn-success btn-block mb-5" to="/login">
							Login to Enroll
						</Link>
					</Card>
				</Container>
			</Col>
		</Row>
	</>
	);
};
