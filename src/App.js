import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

//acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses'
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import Logout from './pages/Logout';
import AddCourse  from './pages/AddCourse'
import UpdateCourse  from './pages/UpdateCourse'
//import the hero section
import './App.css';

// acquire the provider utility inthis entry point
import { UserProvider }from './UserContext';


//implement page routing in our page
//acquire the utilities from react-router-dom
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';
//BrowserRouter -> this is standard library component for router in react. this enable navagition amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
// as -> alias leyword in JS.
//Routes -> it's a new component introduced in V^ of react-router-dom whose task is allow switching between locations.

//JSX component -> self closing tag
//syntax: <element />
import './App.css';

function App() {

  // the role of the provider wa s assign to the app.js wc means all the information that we will declared here will autimatically becomes 'global' scope.
  // initialize a state of the user to provide/identify the status of client who is using the app.
  // id -> to properly ref. the user
  // isAdmin -> role of the user
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    }); // to bind the user to default state.

    // feed the information stated here to the consumers.
    
    // Create a function that will unset/unmount the user
    const unsetUser = () => {
        // clear out the saved data in the local storage. //remove all data in storage
        localStorage.clear(); //remove the token.
        // revert the state of user back tp null.
        setUser({
          id: null,
          isAdmin: null
        });
    }

    // Create a  side effect that will send a request to API collection to  get the identify of the user using the access token that was saved in the browser.
    useEffect(() => {
      // identify the state of the user
      // mount or campaign  the user to the app so that he
      // she will be recognized, using the token.

      // retrieve the token from the storage.
      let token = localStorage.getItem('accessToken');

      // fetch('url', 'options'), keep in mind, in this request er are passing down the token
      fetch('https://frozen-tundra-41775.herokuapp.com/users/details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(convertedData => {

        if (typeof convertedData._id !== "undefined") {
          setUser({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          }); 
        }  
      });
     
    },[user]);
    

    return (
      <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
           <AppNavBar />
           <Routes>
              <Route path='/' element={<Home />} /> 
              <Route path='/register' element={<Register />}  />
              <Route path='/courses' element={<Catalog />} /> 
              <Route path= '/login' element={<LoginPage />} />
              <Route path='/courses/view/:id' element={<CourseView/>} />
              <Route path='/logout' element={<Logout/>} />
              <Route path='/add/courses' element={<AddCourse/>} />
              <Route path='/courses/update' element={<UpdateCourse />}></Route>
              <Route path='*' element={<ErrorPage/>} />
            
           </Routes>
        </Router>
      </UserProvider>
    );
  };

  export default App;
